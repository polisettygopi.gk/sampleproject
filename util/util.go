package util

import (
	"encoding/json"
	"net/http"
)

//ResponseData model for displaying the status
type ResponseData struct {
	Status int
	Meta   interface{}
	Data   interface{}
}

//ResponseMessage for displaying the severity message for http methods
func ResponseMessage(statuscode int) (message interface{}) {
	elements := map[int]map[string]string{
		200: map[string]string{
			"message":  "The request has succeeded. An entity corresponding to the requested resource is sent in the response.",
			"severity": "OK",
		},
		201: map[string]string{
			"message":  "The request has been fulfilled and resulted in a new resource being created.",
			"severity": "CREATED",
		},
		204: map[string]string{
			"message":  "The server successfully processed the request, but is not returning any content.",
			"severity": "NO CONTENT",
		},
		400: map[string]string{
			"message":  "The request could not be understood by the server due to malformed syntax or request.",
			"severity": "BAD REQUEST",
		},
		401: map[string]string{
			"message":  "The request could not be matched. Authorization Required",
			"severity": "UNAUTHORIZED",
		},
		403: map[string]string{
			"message":  "The server did not accept your request.",
			"severity": "FORBIDDEN",
		},
		404: map[string]string{
			"message":  "The server has not found anything matching the request.Boron",
			"severity": "NOT FOUND",
		},
		500: map[string]string{
			"message":  "The server encountered an unexpected condition which prevented it from fulfilling the request.",
			"severity": "INTERNAL SERVER ERROR",
		},
	}
	var codes interface{}
	codes = elements[statuscode]
	return codes
}

//RespondJSON return the http response in json format
func RespondJSON(w http.ResponseWriter, status int, payload interface{}) {
	var res ResponseData
	res.Status = status
	res.Meta = ResponseMessage(status)
	res.Data = payload
	response, err := json.Marshal(res)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(status)
	w.Write([]byte(response))
}

//RespondJSONRaw return the http response in json format
func RespondJSONRaw(w http.ResponseWriter, payload interface{}) {
	response, err := json.Marshal(payload)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
	w.Header().Set("Content-Type", "application/json")

	w.Write([]byte(response))
}

//RespondError return the http method error
func RespondError(w http.ResponseWriter, status int, message string) {
	var res ResponseData
	rescode := ResponseMessage(status)
	res.Status = status
	res.Meta = rescode
	res.Data = message
	response, err := json.Marshal(res)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(status)
	w.Write([]byte(response))
}
