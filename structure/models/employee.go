package models

//Employees model structure
type Employees struct {
	ID      int    `json:"id"`
	Empname string `json:"empName"`
	Salary  int    `json:"salary"`
	Deptid  int    `json:"deptId"`
}

//TableName reruns table name
func (Employees) TableName() string {
	return ("employees")

}
