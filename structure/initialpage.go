package structure

import (
	"fmt"
	"log"
	"net/http"
	"time"

	"github.com/gorilla/mux"
	"github.com/jinzhu/gorm"
)

//Structure structure
type Structure struct {
	Router *mux.Router
	DB     *gorm.DB
}

const (
	//Static constant
	Static = "/templates/"
)

//Initialize connect to database
func (app *Structure) Initialize(dbname string, dbhost string, dbport string, dbuser string, dbpass string) (*gorm.DB, error) {
	dbURI := fmt.Sprintf("dbname=%s host=%s port=%s user=%s  password=%s  sslmode=disable",
		dbname, dbhost, dbport, dbuser, dbpass)

	db, err := gorm.Open("postgres", dbURI)

	if err != nil {
		log.Fatal("Could not connect database")
	}
	db.DB().SetMaxIdleConns(2)
	db.DB().SetMaxOpenConns(100)
	db.DB().SetConnMaxLifetime(5 * time.Minute)
	app.DB = db
	return db, nil
}

//Handlers for handle
func (app *Structure) Handlers() *mux.Router {

	app.Router = mux.NewRouter().StrictSlash(true)
	app.Router.PathPrefix(Static).Handler(http.StripPrefix(Static, http.FileServer(http.Dir("."+Static))))
	app.Router.HandleFunc("/getemployees", app.GetEmployees).Methods("GET")
	return app.Router
}
