package structure

import (
	"fmt"
	"net/http"
	"sampleproject/structure/models"
	"sampleproject/util"

	"github.com/jinzhu/gorm"
)

//GetEmployees function
func (st *Structure) GetEmployees(Response http.ResponseWriter, Request *http.Request) {

	empdetails, err := GetEmployees(st.DB)
	if err != nil {
		fmt.Println(err.Error())
	}
	util.RespondJSON(Response, http.StatusOK, empdetails)
}

//GetEmployees database records fetch
func GetEmployees(db *gorm.DB) (emp []models.Employees, err error) {

	if err = db.Find(&emp).Error; err != nil {
		return nil, err
	}
	return emp, nil

}
