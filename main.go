package main

import (
	"log"
	"net/http"
	"sampleproject/structure"

	_ "github.com/lib/pq"
)

func main() {
	listenPort := ":8000"
	st := structure.Structure{}
	st.Initialize("empdb", "localhost", "5432", "postgres", "postgres")
	http.Handle("/", st.Handlers())
	log.Fatal(http.ListenAndServe(listenPort, st.Router))

}
